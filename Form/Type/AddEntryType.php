<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gibz
 * Date: 08.10.13
 * Time: 2:30
 * To change this template use File | Settings | File Templates.
 */

namespace Gibz\BlogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class AddEntryType extends AbstractType {
    public function BuildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('addEntry')
            ->add('name', 'text', array())
            ->add('text', 'textarea', array())
            ->add('createdAt', 'datetime', array())
            ->add('save', 'submit')
            ->add('reset', 'reset');
    }

    public function getName() {
        return 'addEntry';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Gibz\BlogBundle\Entity\Entry',
        ));
    }
}
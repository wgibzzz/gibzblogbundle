<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gibz
 * Date: 07.10.13
 * Time: 8:03
 * To change this template use File | Settings | File Templates.
 */

namespace Gibz\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Gibz\BlogBundle\Form\Type\AddEntryType;


class BlogController extends Controller {
    public function indexAction() {
        $entries = $this
            ->getDoctrine()
            ->getRepository('GibzBlogBundle:Entry')
            ->findAll();

        return $this->render('GibzBlogBundle:Blog:index.html.twig', array('entries' => $entries));
    }

    public function entryAction($id) {
        $entry = $this
            ->getDoctrine()
            ->getRepository('GibzBlogBundle:Entry')
            ->find($id);

        return $this->render('GibzBlogBundle:Blog:entry.html.twig', array('entry' => $entry));
    }

    public function addAction(Request $request) {
        $task = null;
        $form = $this->createForm(new AddEntryType(), $task);

        return $this->render('GibzBlogBundle:Blog:add.html.twig', array('form' => $form->createView()));
    }
}